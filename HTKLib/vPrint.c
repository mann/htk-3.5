/* ----------------------------------------------------------- */
/*                                                             */
/*                          ___                                */
/*                       |_| | |_/   SPEECH                    */
/*                       | | | | \   RECOGNITION               */
/*                       =========   SOFTWARE                  */
/*                                                             */
/*                                                             */
/* ----------------------------------------------------------- */
/*         File: vPrint.c           Value printing routine      */
/* ----------------------------------------------------------- */

#include "vPrint.h"

/* ------------------- Print NFloat structure  --------------------- */
void PrintNFloatAsRow(NFloat *fltVal, int offset, int count, int rowLen, char *varName) {
    /* sanity check */
    if (!(count > 0 && rowLen > 0))
      return;
    
    int i, j;
    int nrows = (count + rowLen-1) / rowLen;
    int maxInd = offset + count;
    
    for (i = 0; i < nrows; ++i) {
        printf("%s[%d..%d] = %f", varName, offset, offset+rowLen, fltVal[offset]);
        ++offset;
        for (j = 1; j < rowLen; ++j) {
            if (offset >= maxInd) break;
            
            printf(", %f", fltVal[offset]);
            ++offset;
        }
        printf("\n");
    }
}


/* ------------------- Print NVector structure  --------------------- */

void PrintNVector(NVector *vec, int offset, int count, char *vecName) {
    int maxId = offset + count;
  
    /* sanity check */
    if (!(offset >= 0 && count > 0 && maxId <= vec->vecLen))
      return;
  
#ifdef CUDA
    SyncNVectorDev2Host(vec);
#endif
    printf("%s[%d->%d | %lu] = %f", vecName, offset, maxId-1, vec->vecLen, vec->vecElems[offset]);
    ++offset;
    while (offset < maxId) {
        printf(", %f", vec->vecElems[offset++]);
    }
    printf("\n");
}

/* ------------------- Print NMatrix structure  --------------------- */

void PrintNMatrix(NMatrix *mat, int rowOffset, int rowCount, int colOffset, int colCount, char *matName) {
    int maxRow, maxCol, j, elemId;
  
    maxRow = rowOffset + rowCount;
    maxCol = colOffset + colCount;
    
    /* sanity check */
    if (!(rowOffset >= 0 && rowCount > 0 && maxRow <= mat->rowNum))
      return;
    if (!(colOffset >= 0 && colCount > 0 && maxCol <= mat->colNum))
      return;
  
#ifdef CUDA
    SyncNMatrixDev2Host(mat);
#endif
    printf("%s = [%lu x %lu]\n", matName, mat->rowNum, mat->colNum);
    while (rowOffset < maxRow) {
      /* print: matName[row][0 */
      printf("%s[%4d][%d", matName, rowOffset, colOffset);
  
      /* print: ,1,2,3,4] =  */
      for (j = colOffset+1; j < maxCol; ++j) {
        printf(",%d", j);
      }
      printf("] = ");
  
      /* print: elem1, elem2, elem3, elem4] */
      elemId = mat->colNum * rowOffset + colOffset;
      printf(" %f", mat->matElems[elemId]);
      for (j = 1; j < colCount; ++j) {
        printf(", %f", mat->matElems[elemId + j]);
      }
      printf("\n");
  
      rowOffset++;
    }
    printf("\n");
}

/* ------------------------- End of vPrint.c --------------------------- */

