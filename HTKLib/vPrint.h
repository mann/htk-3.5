/* ----------------------------------------------------------- */
/*                                                             */
/*                          ___                                */
/*                       |_| | |_/   SPEECH                    */
/*                       | | | | \   RECOGNITION               */
/*                       =========   SOFTWARE                  */
/*                                                             */
/*                                                             */
/* ----------------------------------------------------------- */
/*         File: vPrint.h      Value printing routine           */
/* ----------------------------------------------------------- */

#ifndef _VPRINT_H_
#define _VPRINT_H_

#include "HMem.h"

/* ------------------- Print NFloat structure  --------------------- */

void PrintNFloatAsRow(NFloat *fltVal, int offset, int count, int rowLen, char *varName);
  

/* ------------------- Print NVector structure  --------------------- */

void PrintNVector(NVector *vec, int offset, int count, char *vecName);


/* ------------------- Print NMatrix structure  --------------------- */

void PrintNMatrix(NMatrix *mat, int rowOffset, int rowCount, int colOffset, int colCount, char *matName);


#endif  /* _VPRINT_H_ */

/* ------------------------- End of vPrint.h --------------------------- */

